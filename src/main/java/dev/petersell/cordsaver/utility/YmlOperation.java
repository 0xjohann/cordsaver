package dev.petersell.cordsaver.utility;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class YmlOperation {

    public static YamlConfiguration getYmlFile(Player player) {
        // get yml from command sender
        File file = new File("plugins//CordsConfig//" + player.getName() + ".yml");
        return YamlConfiguration.loadConfiguration(file);
    }

    // set yml with List<String> value
    public static void setYml(String key, List<String> val, Player player) {
        // get yml from command sender
        File file = new File("plugins//CordsConfig//" + player.getName() + ".yml");
        YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(file);

        // set yml
        yamlConfiguration.set(key, val);

        // save yml
        try {
            yamlConfiguration.save(file);
        } catch (IOException e) {
            player.sendMessage("§7An error occurred while saving the cords.");
            e.printStackTrace();
        }
    }

    // set yml with HashMap<String, String> value
    public static void setYml(String key, HashMap<String, String> val, Player player) {
        // get yml from command sender
        File file = new File("plugins//CordsConfig//" + player.getName() + ".yml");
        YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(file);

        // set yml
        yamlConfiguration.set(key, val);

        // save yml
        try {
            yamlConfiguration.save(file);
        } catch (IOException e) {
            player.sendMessage("§7An error occurred while saving the cords.");
            e.printStackTrace();
        }
    }

    // set yml with String value
    public static void setYml(String key, String val, Player player) {
        // get yml from command sender
        File file = new File("plugins//CordsConfig//" + player.getName() + ".yml");
        YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(file);

        // set yml
        yamlConfiguration.set(key, val);

        // save yml
        try {
            yamlConfiguration.save(file);
        } catch (IOException e) {
            player.sendMessage("§7An error occurred while saving the cords.");
            e.printStackTrace();
        }
    }

    public static Object getYml(String key, Player player) {
        // get yml from command sender
        File file = new File("plugins//CordsConfig//" + player.getName() + ".yml");
        YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(file);

        // return
        return yamlConfiguration.get(key);
    }

    public static List<String> getYmlList(String key, Player player) {
            // get yml from command sender
            File file = new File("plugins//CordsConfig//" + player.getName() + ".yml");
            YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(file);

            // return
            return yamlConfiguration.getStringList(key);
    }
}
