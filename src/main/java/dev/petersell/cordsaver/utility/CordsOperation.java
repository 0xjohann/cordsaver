package dev.petersell.cordsaver.utility;

import dev.petersell.cordsaver.Main;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.boss.KeyedBossBar;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.Bukkit;
import org.bukkit.Sound;

import java.util.*;

public class CordsOperation {
    public static void saveCurrentPosition(Player player, String name) {
        if (name == null || name.isEmpty()) {
            player.sendMessage("§cUsage: /cords save <name>");
            return;
        }
        YamlConfiguration yC = YmlOperation.getYmlFile(player);
        Set<String> keySet = yC.getKeys(true);
        if (keySet.contains(name)) {
            player.sendMessage("§cCords \"" + name + "\" do already exist.");
            return;
        }

        // Check for reserved names
        if (name.equals("settings") || name.equals("tmp") || name.equals("airdrop") || name.equals("#here")) {
            player.sendMessage(ChatColor.RED + "\"settings\", \"tmp\", \"#here\" and \"airdrop\" are reserved names.");
            return;
        }

        // get Current Location
        Location pos = player.getLocation();

        // building List<String>
        List<String> cords = new ArrayList<>();
        cords.add(pos.getBlockX() + "");
        cords.add(pos.getBlockY() + "");
        cords.add(pos.getBlockZ() + "");
        cords.add(pos.getWorld().getName());

        // write List<String> to yml file from command sender
        YmlOperation.setYml(name, cords, player);

        String dimensionFormated;
        String dimension = pos.getWorld().getName();
        if ("world_nether".equals(dimension)) {
            dimensionFormated = ChatColor.DARK_RED + "Nether";
        } else if ("world_the_end".equals(dimension)) {
            dimensionFormated = ChatColor.DARK_PURPLE + "The End";
        } else {
            dimensionFormated = ChatColor.DARK_GREEN + "Overworld";
        }

        player.sendMessage("Cords saved as §e" + name + "§f at X:§e " + pos.getBlockX() + " §fY: §e" + pos.getBlockY() + " §fZ: §e" + pos.getBlockZ() + "§f in the dimension " + dimensionFormated);
    }

    public static void navigate(Player player, String cord) {

        if (cord.isEmpty()) {
            player.sendMessage("§cWrong usage.");
            return;
        }

        if (!YmlOperation.getYmlFile(player).getKeys(false).contains(cord)) {
            player.sendMessage("§c\"" + cord + "\" does not exist.");
            return;
        }

        List<String> cordList = YmlOperation.getYmlList(cord, player);

        // get x, y, z and store as String
        String xS = cordList.get(0);
        String yS = cordList.get(1);
        String zS = cordList.get(2);

        if (xS == null || yS == null ||
                zS == null) {
            player.sendMessage("§cSaved cord \"" + cord + "\" is corrupt.");
            return;
        }

        // convert x, z to int
        int x = Integer.parseInt(xS);
        int z = Integer.parseInt(zS);

        // get distance between loaded cords and own position
        double distance = getDistance(x, z, player.getLocation().getBlockX(), player.getLocation().getBlockZ());

        // store temporary data in yml
        HashMap<String, String> tmp = new HashMap<>();
        tmp.put("loaded", cord);
        tmp.put("distance", distance + "");
        YmlOperation.setYml("tmp", tmp, player);

        // remove bossbar if a new one is loaded
        BossBar bar;
        NamespacedKey key = new NamespacedKey(Main.getPlugin(), player.getName().toLowerCase());
        if (Bukkit.getBossBar(new NamespacedKey(Main.getPlugin(), player.getName())) != null) {
            KeyedBossBar bossbar = Bukkit.getBossBar(key);
            if (bossbar != null) {
                Objects.requireNonNull(Bukkit.getBossBar(key)).removePlayer(player);
            } else {
                player.sendMessage("§cAn error occured executing the command.");
                return;
            }
        }

        // get right color for dimension
        BarColor dimensionColor;
        String dimension = null;
        try {
            dimension = (String) Objects.requireNonNull(YmlOperation.getYmlFile(player).getList(cord)).get(3);
        } catch (NullPointerException e) {
            player.sendMessage("§cAn error occured executing the command.");
        }
        if ("world_nether".equals(dimension)) {
            dimensionColor = BarColor.RED;
        } else if ("world_the_end".equals(dimension)) {
            dimensionColor = BarColor.PURPLE;
        } else {
            dimensionColor = BarColor.GREEN;
        }

        String title = "X: §e" + xS + " §fY: §e" + yS + " §fZ: §e" + zS;

        // write bossbar title
        bar = Bukkit.createBossBar(key, title, dimensionColor, BarStyle.SOLID);

        // render bossbar
        bar.setProgress(0.0);
        bar.addPlayer(player);
        bar.setVisible(true);
    }

    public static void clear(Player player) {
        // get namespacekey for player and remove bossbar
        NamespacedKey key = new NamespacedKey(Main.getPlugin(), player.getName().toLowerCase());
        KeyedBossBar bossbar = Bukkit.getBossBar(key);
        if (bossbar != null) {
            Objects.requireNonNull(Bukkit.getBossBar(key)).removePlayer(player);
        } else {
            player.sendMessage("§cAn error occured executing the command.");
            return;
        }
        YmlOperation.setYml("tmp.loaded", (String) null, player);
    }

    public static void save(Player player, String[] args) {
        YamlConfiguration yC = YmlOperation.getYmlFile(player);
        Set<String> keySet = yC.getKeys(true);
        if (keySet.contains(args[1])) {
            player.sendMessage("§cCords \"" + args[1] + "\" do already exist.");
            return;
        }
        if (args.length != 6) {
            player.sendMessage("§cIncorrect Usage.");
            return;
        }
        if (args[1].equals("settings") || args[1].equals("tmp") || args[1].equals("airdrop") || args[1].equals("#here")) {
            player.sendMessage(ChatColor.RED + "\"settings\", \"tmp\", \"#here\" and \"airdrop\" are reserved names.");
            return;
        }
        // add x, y, z and world to List<String>
        List<String> cords = new ArrayList<>();
        cords.add(args[2] + "");
        cords.add(args[3] + "");
        cords.add(args[4] + "");
        if (args[5].equals("o") || args[5].equalsIgnoreCase("overworld")) {
            cords.add("world");
        } else if (args[5].equals("n") || args[5].equalsIgnoreCase("nether")) {
            cords.add("world_nether");
        } else if (args[5].equals("e") || args[5].equalsIgnoreCase("end")) {
            cords.add("world_the_end");
        } else {
            player.sendMessage("§7An error occurred while saving the cords.");
            player.sendMessage("§b/cords add <Name> <X> <Y> <Z> <World (o for Overwolrd, n for Nether, e for The End)>");
        }

        // send confirm message
        String dimension;
        ChatColor dimensionColor;
        if (args[5].equals("n") || args[5].equalsIgnoreCase("Nether")) {
            dimension = "Overworld";
            dimensionColor = ChatColor.DARK_RED;
        } else if (args[5].equals("e") || args[5].equalsIgnoreCase("End")) {
            dimension = "End";
            dimensionColor = ChatColor.DARK_PURPLE;
        } else {
            dimension = "Nether";
            dimensionColor = ChatColor.DARK_GREEN;
        }

        player.sendMessage("Cords saved as §e" + args[1] + "§f at X:§e " + args[2] + " §fY: §e" + args[3] + " §fZ: §e" + args[4] + "§f in the dimension " + dimensionColor + dimension);

        YmlOperation.setYml(args[1], cords, player);
    }

    public static void rename(Player player, String name, String newName) {
        if (!YmlOperation.getYmlFile(player).getKeys(false).contains(name)) {
            player.sendMessage("§c\"" + name + "\" does not exist.");
            return;
        }

        if (YmlOperation.getYmlFile(player).contains(name)) {
            if (newName.equals("settings") || newName.equals("tmp")  || newName.equals("#here") || newName.equals("airdrop")) {
                player.sendMessage(ChatColor.RED + "\"settings\", \"tmp\", \"#here\" and \"airdrop\" are reserved names.");
                return;
            }
            if (!YmlOperation.getYmlFile(player).contains(newName)) {

                // set value for new entry to value from old entry
                YmlOperation.setYml(newName, (List) YmlOperation.getYml(name, player), player);
                YmlOperation.setYml(name, (List) null, player);
                player.sendMessage("Cords renamed from " + ChatColor.YELLOW + name + ChatColor.WHITE + " to " + ChatColor.YELLOW + newName + ChatColor.WHITE + ".");
            }
        }
    }

    public static void delete(Player player, String name) {
        if (!YmlOperation.getYmlFile(player).getKeys(false).contains(name)) {
            player.sendMessage("§c\"" + name + "\" does not exist.");
            return;
        }

        if (YmlOperation.getYmlFile(player).contains(name) && !name.equals("settings") && !name.equals("tmp")) {
            YmlOperation.setYml(name, (String) null, player);
            player.sendMessage("Cords " + ChatColor.YELLOW + name + ChatColor.WHITE + " deleted.");
        }
    }

    public static void airDrop(Player sender, String[] args) {
        if(!(args.length == 3)) {
            sender.sendMessage("§§cIncorrect Usage.");
            return;
        }
        String cordsName = args[1];
        Player receiver = Bukkit.getPlayer(args[2]);
        YamlConfiguration yC = YmlOperation.getYmlFile(sender);

        if (receiver == null || cordsName == null) {
            sender.sendMessage("§cIncorrect Usage.");
            return;
        }

        if (!Bukkit.getOnlinePlayers().contains(receiver)) {
            sender.sendMessage("§cIncorrect Usage.");
            return;
        }

        if (!yC.getKeys(true).contains(cordsName)) {
            sender.sendMessage("The cord \"" + cordsName + "\" does not exist.");
            return;
        }

        YmlOperation.setYml("airdrop.sender", sender.getName(), receiver);
        YmlOperation.setYml("airdrop.cords", cordsName, receiver);

        sender.sendMessage("§aYou sent \"" + cordsName + "\" to " + receiver.getName() + ".");
        receiver.playSound(receiver.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER, (float) 1.0, (float) 1.0);
        receiver.sendMessage("§aYou received cords \"" + cordsName + "\" from " + sender.getName() + ". Type /cords accept to accept or type /cords accept navigate to directly navigate to the position.");
    }

    public static void acceptAirDrop(Player player, String[] args) {
        YamlConfiguration yC = YmlOperation.getYmlFile(player);
        if(args[1].equals("#here") || args[1].equals("airdrop") || args[1].equals("tmp") || args[1].equals("settings")) {
            player.sendMessage(ChatColor.RED + "\"settings\", \"tmp\", \"#here\" and \"airdrop\" are reserved names.");
            return;
        }
        if(!yC.getKeys(true).contains("airdrop")) {
            player.sendMessage("§cNo cords have been shared with you yet.");
            return;
        }
        String sender = yC.getString("airdrop.sender");
        String cords = yC.getString("airdrop.cords");
        YamlConfiguration yCSender = YmlOperation.getYmlFile(Bukkit.getPlayer(sender));
        if(!yCSender.getKeys(true).contains(cords)) {
            player.sendMessage("§cA problem occurred getting the cords.");
            return;
        }

        List<String> cordsList = yCSender.getStringList(cords);
        if(args.length >= 2) {
            if(args[1].equals("navigate")) {
                //TOOO seperate navigation method
                player.sendMessage("Please use another name as \"navigate\".");
            } else if(yC.getKeys(true).contains(args[1])) {
                player.sendMessage("§cYou already saved cords with the name \"" + args[1] + "\".");
                return;
            }
            YmlOperation.setYml(args[1], cordsList, player);
            player.sendMessage("§aYou accepted cords \"" + cords + "\" from " + sender + ". They were saved as \"" + args[1] + "\".");
        } else {
            if(yC.getKeys(true).contains(cords)) {
                player.sendMessage("§cYou already saved cords with the name \"" + cords + "\". Rename your existing cords and accept again.");
                return;
            }
            YmlOperation.setYml(cords, cordsList, player);
            player.sendMessage("§aYou accepted cords \"" + cords + "\" from " + sender + ".");
        }
        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, (float) 1.0, (float) 1.0);
    }

    public static void broadcast(Player player, String cord) {
        YamlConfiguration yC = YmlOperation.getYmlFile(player);
        Set<String> keys = yC.getKeys(true);

        if (!cord.equals("settings") && !cord.equals("tmp") && !cord.equals("airdrop")) {

            if(!keys.contains(cord) && !cord.equals("#here")) {
                player.sendMessage("§cCords \"" + cord + "\" do not exist.");
                return;
            }

            List<String> cordList;

            if(cord.equals("#here")) {
                String hereX = player.getLocation().getBlockX() + "";
                String hereY = player.getLocation().getBlockY() + "";
                String hereZ = player.getLocation().getBlockZ() + "";
                String hereDimension = player.getLocation().getWorld().getName();
                cordList = Arrays.asList(hereX, hereY, hereZ, hereDimension);
            } else {
                cordList = YmlOperation.getYmlList(cord, player);
            }

            // get color for dimension
            ChatColor dimensionColor;
            boolean knownDimension = true;
            String dimension = cordList.get(3);
            if ("world_nether".equals(dimension)) {
                dimensionColor = ChatColor.DARK_RED;
            } else if ("world_the_end".equals(dimension)) {
                dimensionColor = ChatColor.DARK_PURPLE;
            } else if ("world".equals(dimension)) {
                dimensionColor = ChatColor.DARK_GREEN;
            } else {
                dimensionColor = ChatColor.GRAY;
            }

            // if cord is corrupt
            if(
                cordList.get(0) == null ||
                cordList.get(1) == null ||
                cordList.get(2) == null
            ) {
                knownDimension = false;
            }

            // get string x, y, z and format
            String x = ""; String y = ""; String z = "";
            if (knownDimension) {
                x = dimensionColor + cordList.get(0);
                y = dimensionColor + cordList.get(1);
                z = dimensionColor + cordList.get(2);
            } else {
                player.sendMessage("§cSaved cord \"" + cord + "\" is corrupt.");
            }

            // format name


            // send formatted cords
            for(Player current : Bukkit.getOnlinePlayers()) {
                String name = Settings.getColor(current) + cord;
                current.sendMessage(player.getName() + " broadcasted cord " + name + "§r: " + dimensionColor + x + "§8, " + y + "§8, " + z);
            }
        }
    }

    public static double getDistance(int x1, int z1, int x2, int z2) {
        int x = x1 - x2;
        int z = z1 - z2;

        return Math.sqrt(Math.pow(x, 2) + Math.pow(z, 2));
    }
}

