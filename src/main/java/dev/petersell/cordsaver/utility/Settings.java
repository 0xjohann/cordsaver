package dev.petersell.cordsaver.utility;

import org.bukkit.entity.Player;

public class Settings {
    public static String getColor(Player player) {
        return (String) YmlOperation.getYml("settings.color", player);
    }

    public static void setColor(Player player, String[] args) {
        return;
    }

    public static void toggleDebug(Player player) {
        if (YmlOperation.getYml("settings.debug", player).equals("true")) YmlOperation.setYml("settings.debug", "false", player);
        else YmlOperation.setYml("settings.debug", "true", player);
    }
}