package dev.petersell.cordsaver.utility;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;

public class MessageSender {
    public static void displayAllSavedCords(Player player) {

        // send title

        String color = Settings.getColor(player);

        player.sendMessage(color + "--=={ All Cords }==--");
        player.sendMessage(" ");

        for(String current : YmlOperation.getYmlFile(player).getKeys(true)) {
            if (!current.contains("settings") && !current.contains("tmp") && !current.contains("airdrop")) {

                List<String> cordList = YmlOperation.getYmlList(current, player);

                // get color for dimension
                ChatColor dimensionColor;
                boolean knownDimension = true;
                String dimension = cordList.get(3);
                if ("world_nether".equals(dimension)) {
                    dimensionColor = ChatColor.DARK_RED;
                } else if ("world_the_end".equals(dimension)) {
                    dimensionColor = ChatColor.DARK_PURPLE;
                } else if ("world".equals(dimension)) {
                    dimensionColor = ChatColor.DARK_GREEN;
                } else {
                    dimensionColor = ChatColor.GRAY;
                }

                // if cord is corrupt
                if(
                    cordList.get(0) == null ||
                    cordList.get(1) == null ||
                    cordList.get(2) == null
                ) {
                    knownDimension = false;
                }

                // building blocks
                String minus = ChatColor.DARK_GRAY + "- ";
                String pipe = " §8| ";
                String comma = "§8, ";

                // get string x, y, z and format
                String cords;
                if (knownDimension) {
                    String x = dimensionColor + cordList.get(0);
                    String y = dimensionColor + cordList.get(1);
                    String z = dimensionColor + cordList.get(2);
                    cords = x + comma + y + comma + z;
                } else {
                    cords = ChatColor.RED + "corrupt";
                }

                // format name
                String name = color + current;

                // send formatted cords
                player.sendMessage(minus + name + pipe + cords);
            }
        }
    }

    public static void sendUsage(Player player) {
        String color = (String) YmlOperation.getYml("settings.color", player);
        player.sendMessage(" ");
        player.sendMessage(color + "--=={ Usage }==--");
        player.sendMessage(" ");
        player.sendMessage(color + " - help | h");
        player.sendMessage(ChatColor.GRAY + "    - show this list");
        player.sendMessage(" ");
        player.sendMessage(color + " - save | s");
        player.sendMessage(ChatColor.GRAY + "    - save your current position");
        player.sendMessage(" ");
        player.sendMessage(color + " - navigate | n");
        player.sendMessage(ChatColor.GRAY + "    - show position in bossbar and navigate to them");
        player.sendMessage(" ");
        player.sendMessage(color + " - clear | c");
        player.sendMessage(ChatColor.GRAY + "    - clear the bossbar");
        player.sendMessage(" ");
        player.sendMessage(color + " - rename | r");
        player.sendMessage(ChatColor.GRAY + "    - rename a position");
        player.sendMessage(" ");
        player.sendMessage(color + " - delete | d");
        player.sendMessage(ChatColor.GRAY + "    - delete a saved position");
        player.sendMessage(" ");
        player.sendMessage(color + " - saveat <name> <x> <y> <z> <o (overworld), n (nether), e (end)>");
        player.sendMessage(ChatColor.GRAY + "    - save cords with there coordinates");
        player.sendMessage(" ");
        player.sendMessage(color + " - airdrop | ad");
        player.sendMessage(ChatColor.GRAY + "    - send a cord to another player");
        player.sendMessage(" ");
        player.sendMessage(color + " - broadcast | b");
        player.sendMessage(ChatColor.GRAY + "    - send a cord to all players");
        player.sendMessage(" ");
    }

}
