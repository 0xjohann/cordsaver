package dev.petersell.cordsaver.commands;

import dev.petersell.cordsaver.utility.YmlOperation;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CordsCompletion implements TabCompleter {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {


        if (sender instanceof Player && (label.equals("cords") || label.equals("c"))) {
            Player player = (Player) sender;
            switch (args.length) {
                case 1: return StringUtil.copyPartialMatches(args[0], Arrays.asList("saveat", "save", "clear", "navigate", "rename", "delete", "help", "settings", "airdrop", "broadcast"), new ArrayList<>());
                case 2: {
                    switch (args[0]) {
                        case "navigate": case "n": case "rename":  case "r": case "delete": case "d": case "airdrop": case "ad": case "broadcast": case "b": {
                            List<String> names = new ArrayList<>();
                            for (String i : YmlOperation.getYmlFile(player).getKeys(false)) {
                                if (!(i.equals("tmp") || i.equals("settings") || i.equals("airdrop"))) names.add(i);
                            }
                            if(args[0].equals("broadcast") || args[0].equals("b")) {
                                names.add("#here");
                            }
                            return StringUtil.copyPartialMatches(args[1], names, new ArrayList<>());
                        }

                        case "settings":
                            return StringUtil.copyPartialMatches(args[1], Arrays.asList("color", "debug"), new ArrayList<>());
                    }
                    break;
                }
                case 3: {
                    switch (args[0]) {
                        case "settings": {
                            switch (args[1]) {
                                case "color": {
                                    List<String> names = new ArrayList<>();
                                    for (ChatColor i : ChatColor.values()) {
                                        if (!Arrays.asList("ITALIC", "STRIKETHROUGH", "RESET", "BOLD", "UNDERLINE", "MAGIC").contains(i.name())) names.add(i.name().toLowerCase());
                                    }

                                    return StringUtil.copyPartialMatches(args[2], names, new ArrayList<>());
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
                case 6: return StringUtil.copyPartialMatches(args[5], Arrays.asList("overworld", "nether", "end"), new ArrayList<>());
            }
        }
        return null;
    }
}

