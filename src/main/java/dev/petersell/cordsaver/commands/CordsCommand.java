package dev.petersell.cordsaver.commands;

import dev.petersell.cordsaver.utility.CordsOperation;
import dev.petersell.cordsaver.utility.MessageSender;
import dev.petersell.cordsaver.utility.Settings;
import dev.petersell.cordsaver.utility.YmlOperation;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CordsCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            // Set standard config namespaces if not existed
            if(YmlOperation.getYml("settings", player) == null) {
                YmlOperation.setYml("settings.color", "§6", player);
                YmlOperation.setYml("settings.debug", "false", player);
            }

            if(args.length == 0 ) {
                MessageSender.displayAllSavedCords(player);
            } else {
                switch (args[0].toLowerCase()) {
                    case "save": case "s":  {
                        if (!verifyArgs(args, 1, player)) return false;
                        CordsOperation.saveCurrentPosition(player, args[1]);
                        break;
                    }
                    case "clear": case "c": {
                        CordsOperation.clear(player);
                        break;
                    }
                    case "saveat": {
                        CordsOperation.save(player, args);
                        break;
                    }
                    case "navigate": case "n": {
                        if (!verifyArgs(args, 1, player)) return false;
                        CordsOperation.navigate(player, args[1]);
                        break;
                    }
                    case "settings": {
                        if (!verifyArgs(args, 1, player)) return false;
                        switch (args[1].toLowerCase()) {
                            case "debug": Settings.toggleDebug(player);
                            case "color": Settings.setColor(player, args);
                        }
                        break;
                    }
                    case "help": case "h": {
                        MessageSender.sendUsage(player);
                        break;
                    }
                    case "rename": case "r": {
                        if (!verifyArgs(args, 2, player)) return false;
                        CordsOperation.rename(player, args[1], args[2]);
                        break;
                    }
                    case "delete": case "d": {
                        if (!verifyArgs(args, 1, player)) return false;
                        CordsOperation.delete(player, args[1]);
                        break;
                    }
                    case "airdrop": case "ad": {
                        CordsOperation.airDrop(player, args);
                        break;
                    }
                    case "accept": case "a": {
                        if (!verifyArgs(args, 1, player)) return false;
                        CordsOperation.acceptAirDrop(player, args);
                        break;
                    }
                    case "broadcast": case "b": {
                        if (!verifyArgs(args, 1, player)) return false;
                        CordsOperation.broadcast(player, args[1]);
                    }
                }
            }
        }
        return false;
    }

    private boolean verifyArgs(String[] args, int i, Player player) {
        if (args.length < i + 1) {
            player.sendMessage("§cWrong usage. Use /help for syntax.");
            return false;
        } else return true;
    }
}
