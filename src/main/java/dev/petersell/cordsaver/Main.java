package dev.petersell.cordsaver;

import dev.petersell.cordsaver.commands.CordsCommand;
import dev.petersell.cordsaver.commands.CordsCompletion;
import dev.petersell.cordsaver.listener.MovementListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class Main extends JavaPlugin {

    private static Main plugin;

    @Override
    public void onEnable() {
        plugin = this;

        getCommand("cords").setExecutor(new CordsCommand());
        getCommand("cords").setTabCompleter(new CordsCompletion());
        getCommand("c").setExecutor(new CordsCommand());
        getCommand("c").setTabCompleter(new CordsCompletion());


        Bukkit.getPluginManager().registerEvents(new MovementListener(), plugin);
    }

    public static Main getPlugin() {
        return plugin;
    }
}

/* TODO
- life cords in bossbar
- gespeicherte cords in globalchat und msg
- aktuelle cords in globalchat und msg
- cords ordner
 */