package dev.petersell.cordsaver.listener;

import dev.petersell.cordsaver.Main;
import dev.petersell.cordsaver.utility.CordsOperation;
import dev.petersell.cordsaver.utility.YmlOperation;
import org.bukkit.*;
import org.bukkit.boss.BossBar;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MovementListener implements Listener {

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        File file = new File("plugins//CordsConfig//" + event.getPlayer().getName() + ".yml");
        YamlConfiguration yC = YamlConfiguration.loadConfiguration(file);
        String current = yC.getString("tmp.loaded");

        if (current != null) {
            //Get cords from saves and generate Location
            List<String> list = yC.getStringList(current);
            String xS = list.get(0);
            String yS = list.get(1);
            String zS = list.get(2);

            int x = Integer.parseInt(xS);
            int y = Integer.parseInt(yS);
            int z = Integer.parseInt(zS);
            World world = Bukkit.getWorld(list.get(3));
            Location targetLocation = new Location(world, x, y, z);

            double currentDistance = CordsOperation.getDistance(x, z, event.getPlayer().getLocation().getBlockX(), event.getPlayer().getLocation().getBlockZ());
            double distance = Double.parseDouble((String) YmlOperation.getYml("tmp.distance", event.getPlayer()));

            double percent = currentDistance / distance;

            if (percent > 1.0) {
                distance = CordsOperation.getDistance(x, z, event.getPlayer().getLocation().getBlockX(), event.getPlayer().getLocation().getBlockZ());
                YmlOperation.setYml("tmp.distance", String.valueOf(distance), event.getPlayer());
                percent = 1.0;
            }

            ABVector playerLookV = new ABVector(event.getPlayer().getLocation().getDirection().getX(), event.getPlayer().getLocation().getDirection().getZ()); //Blickrichtung des Spielers
            ABVector locV = new ABVector(targetLocation.getX(), targetLocation.getZ()); //Ortsvektor der Zielposition
            ABVector playerV = new ABVector(event.getPlayer().getLocation().getX(), event.getPlayer().getLocation().getZ()); //Ortsvektor der Position des Spielers

            BossBar bar = Bukkit.getBossBar(new NamespacedKey(Main.getPlugin(), event.getPlayer().getName().toLowerCase()));
            bar.setTitle((((int) currentDistance <= 10) ? ChatColor.GREEN + "✓" + ChatColor.WHITE : locV.getDirectionNavigation(playerLookV, playerV)) + " | " + "X: §e" + xS + " §fY: §e" + yS + " §fZ: §e" + zS + ChatColor.WHITE + " | " + (int) currentDistance + "m");
            bar.setProgress(1 - percent);
        }
    }

    public class ABVector {

        private double a;
        private double b;

        public ABVector(double a, double b) {
            this.a = a;
            this.b = b;
        }

        public double getA() {
            return this.a;
        }

        public double getB() {
            return this.b;
        }

        public double getLength() {
            return Math.sqrt(Math.pow(this.a, 2) + Math.pow(this.b, 2));
        }

        public double getDotProduct(ABVector v) {
            return this.getA() * v.getA() + this.getB() * v.getB();
        }

        public double angleBetween(ABVector v) {
            double length1 = this.getLength();
            double length2 = v.getLength();
            double dop = this.getDotProduct(v);
            return Math.acos(dop / (length1 * length2)) * (180 / Math.PI);
        }

        //Muss auf den Ortsvektor der Position des Spielers angewendet werden
        public ABVector relativePlayerVector(ABVector location) {
            double a = this.getA() - location.getA();
            double b = this.getB() - location.getB();
            return new ABVector(a, b);
        }

        //Muss auf den Vektor der aktuellen Blickrichtung des Spielers angewendet werden
        public double angleRealToRel(ABVector relPlayerV) {
            double testAngle = Math.atan2(relPlayerV.getB(), relPlayerV.getA()) - Math.atan2(this.getB(), this.getA());
            if (testAngle < 0) {
                return (testAngle * (180 / Math.PI) + 360);
            } else if (testAngle == 0) {
                return 0.0;
            } else if (testAngle > 0) {
                return testAngle * (180 / Math.PI);
            } else
                return 400.0;
        }

        //Muss auf den Vektor der Zielposition angewendet werden
        public String getDirectionNavigation(ABVector view, ABVector position) {

            ABVector playerToLocationV = this.relativePlayerVector(position);
            Double angle = view.angleRealToRel(playerToLocationV);
            for (List<Double> current : getNavigationDictionary().keySet()) {
                Double min = current.get(0);
                Double max = current.get(1);
                if (min < angle && angle < max) return getNavigationDictionary().get(current);
            }
            return "…";

        }

        private Map<List<Double>, String> getNavigationDictionary() {

            Map<List<Double>, String> directions = new HashMap<List<Double>, String>();
            List<Double> range1 = Arrays.asList(0.0, 5.0);
            directions.put(range1, "↑"); // geradeaus
            List<Double> range2 = Arrays.asList(5.0, 45.0);
            directions.put(range2, "⬈"); // halbrechts
            List<Double> range3 = Arrays.asList(45.0, 162.0);
            directions.put(range3, "→"); // rechts
            List<Double> range4 = Arrays.asList(162.0, 198.0);
            directions.put(range4, "↓"); // umdrehen
            List<Double> range5 = Arrays.asList(198.0, 315.0);
            directions.put(range5, "←"); // links
            List<Double> range6 = Arrays.asList(315.0, 355.0);
            directions.put(range6, "⬉"); // halblinks
            List<Double> range7 = Arrays.asList(355.0, 360.0);
            directions.put(range7, "↑"); // geradeaus
            return directions;

        }

    }
}
